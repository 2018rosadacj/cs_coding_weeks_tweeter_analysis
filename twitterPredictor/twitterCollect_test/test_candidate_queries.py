import pytest
import cs_coding_weeks_tweeter_analysis.twitterPredictor.twitterCollect.candidate_queries as target

def test():
    assert (target.get_candidate_queries(1,"../candidateData") != -1)#Check if the function can open the file, if it return -1 it means that it couldn't
    assert (len(target.get_candidate_queries(1,"../candidateData")) != 0) #Check if the queries are not empty
