from cs_coding_weeks_tweeter_analysis.twitterPredictor.twitterCollect.collect_candidate_tweet_activity import *       #Remember to change all imports
from cs_coding_weeks_tweeter_analysis.twitterPredictor.twitterCollect.dataframe import *
from pytest import *


def test_collect():
    tweets = get_replies_to_candidate("EmmanuelMacron")
    data =  convert_2_dataframe(tweets)
    assert 'text' in data.columns
