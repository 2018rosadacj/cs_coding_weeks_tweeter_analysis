from tweepy.streaming import StreamListener
import cs_coding_weeks_tweeter_analysis.twitterPredictor.twitterCollect.twitter_connection_setup as connect
import tweepy
import json

class StdOutListener(StreamListener):
    def __init__(self, limit):
        self.counter = 0
        self.limit = limit
        self.tweets = []

    def on_data(self, data):
        print(data)
        dict = json.loads(data)
        self.tweets.append(dict)  #Convert after to json!!!!!!!

        self.counter+=1
        if self.counter >= self.limit:
            return False
        else:
            return True

    def on_error(self, status):
        if  str(status) == "420":
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True




def collect_by_streaming(user_id, limit=1):

    connexion = connect.twitter_setup()
    listener_candidate = StdOutListener(limit = limit)
    stream=tweepy.Stream(auth = connexion.auth, listener=listener_candidate)
    stream.filter(track=[user_id])

    # listener_answers = StdOutListener(limit = limit)
    # stream=tweepy.Stream(auth = connexion.auth, listener=listener_answers)
    # query = 'to:'+user_id
    # stream.filter(track=[query]) The problem is here

    print(listener_candidate.tweets)
    # print(listener_answers.tweets)

collect_by_streaming("EmmanuelMacron")
