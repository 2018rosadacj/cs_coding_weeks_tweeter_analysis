import pytest
import cs_coding_weeks_tweeter_analysis.twitterPredictor.twitterCollect.twitter_connection_setup as test

def test_function():
    """
    Test the API's connection function
    """
    assert (test.twitter_setup() != None)
    return
