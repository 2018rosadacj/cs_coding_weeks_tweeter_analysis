import twitter_connection_setup as connect
import candidate_queries as request

def get_tweets_from_candidates_search_queries(queries, twitter_api):
    """
    Description

    Input:  queries - list of keywords to request the queries;
            twitter_api - an api connection object
    Returns: list of tweets for each query
    """
    tweets = []

    for query in queries:
        tweets.append(twitter_api.search(query,language="french",rpp=10))

    return tweets

if __name__ =="__main__":
    connexion = connect.twitter_setup()
    queries = request.get_candidate_queries(1,"../candidateData")

    get_tweets_from_candidates_search_queries(queries,connexion)
