def get_candidate_queries(num_candidate, file_path):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag
    files
    :param type: type of the keyword, either "keywords" or "hashtags"
    :return: (list) a list of string queries that can be done to the search API independently
    """
    try:
        key_file = open(file_path+"/"+"keywords_candidate_"+str(num_candidate)+".txt", "r") #Gather keywords
        hash_file = open(file_path+"/"+"hashtag_candidate_"+str(num_candidate)+".txt", "r") #Gather hashtags
    except IOError:
        print("Could not open files")
        return -1

    keywords = key_file.readlines()
    hashtags = hash_file.readlines()

    for i in range(len(keywords)):
        keywords[i] = keywords[i][:-1]              #Remove "\n" in the end
    for i in range(len(hashtags)):
        hashtags[i] = hashtags[i][:-1]              #Remove "\n" in the end

    # print(keywords)
    # print(hashtags)

    query = keywords+hashtags

    key_file.close()
    hash_file.close()

    return query

if __name__ == "__main__":
    get_candidate_queries(1,"../candidateData")
