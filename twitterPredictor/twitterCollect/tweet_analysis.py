from collect_candidate_tweet_activity import *
from dataframe import *

tweets = get_replies_to_candidate("EmmanuelMacron")
data = convert_2_dataframe(tweets)

rt_max  = np.max(data['RTs'])
rt  = data[data.RTs == rt_max].index[0]

# Max RTs:
print("The tweet with more retweets is: \n{}".format(data['text'][rt]))
print("Number of retweets: {}".format(rt_max))
