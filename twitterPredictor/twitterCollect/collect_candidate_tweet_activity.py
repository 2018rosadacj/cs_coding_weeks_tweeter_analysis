import twitter_connection_setup as connect

def get_replies_to_candidate(user_id):
    connexion = connect.twitter_setup()
    replies=[]

    for target_tweet in connexion.user_timeline(id = user_id,language="fr",rpp=100):
        # print("///////////////////////////////////////")
        # print(target_tweet.text)
        # print("///////////////////////////////////////")

        query = 'to:'+user_id   #query that will be used to locate the replies
        # print(query)

        for reply_tweet in connexion.search(q=query, since_id=target_tweet.id, result_type='recent',timeout=999999,languague="fr"):

            if hasattr(reply_tweet, 'in_reply_to_status_id_str'):   #check if the tweet has an attribute reply
                # si c'ets une reponse au tweet actuel (full_tweet) du candidat
                if (reply_tweet.in_reply_to_status_id_str==target_tweet.id_str):    #compare the reply_id with the tweet id
                    replies.append(reply_tweet)
                    # print(reply_tweet.text)

    # print(replies)
    return replies

def get_retweets_of_candidate(user_id):
    connexion = connect.twitter_setup()
    retweets=[]

    for target_tweet in connexion.user_timeline(id = user_id,language="fr",rpp=100):
        # print("///////////////////////////////////////")
        # print(target_tweet.text)
        # print("///////////////////////////////////////")

        rt_query = "RT @{}:".format(user_id)

        for retweet in connexion.search(q=rt_query, since_id=target_tweet.id, result_type='recent',timeout=999999,languague="fr"):

            if hasattr(retweet, 'retweeted_status'):   #check if the tweet has an attribute retweet
                if (retweet.retweeted_status.id_str==target_tweet.id_str):    #compare the retweet_id with the tweet id
                    retweets.append(retweet)
                    # print(retweet.text)

    # print(retweets)
    return retweets

if __name__ == "__main__":
    get_replies_to_candidate("EmmanuelMacron")
    get_retweets_of_candidate("EmmanuelMacron")
