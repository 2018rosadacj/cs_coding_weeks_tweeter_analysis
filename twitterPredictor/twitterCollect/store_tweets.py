import json
from collect_candidate_tweet_activity import *

def store_tweets(tweets,filename):
    """
    Input: list of tweets (Status objects, or sometimes dictionaries), name of the file
    """
    usefull_attributes = ["created_at","id_str","text","entities","retweet_count","favorite_count"]

    with open("{}.json".format(filename), "w") as write_file:

        for item in tweets:

            keys = list(item._json.keys())
            print("//////////////////////////")
            print(item)
            print("//////////////////////////")
            for attribute in keys:
                if attribute not in usefull_attributes:  #Compare if the attribute is in the lis of usefull atributes

                    item._json.pop(attribute)             #Delete the unwanted attribute

            json.dump(item._json, write_file)                 #Writes in the file #Change this to a list
            write_file.write('\n')

if __name__ == "__main__":
    tweets = get_replies_to_candidate("EmmanuelMacron")
    store_tweets(tweets,"EmmanuelMacron")
