from textblob import *
from collect_candidate_tweet_activity import *

def get_unique_words(tweets):
    """
    Input: list of tweets
    Output: list of unique words
    """

    words = []
    for item in tweets:
        try:
            tweet = TextBlob(item.text)
            tweet = tweet.translate(to='en')
        except:
            tweet = TextBlob(item.text)

        content = tweet.words
        lemmatized = []
        for word in content:
            w = Word(word)
            lemmatized.append(w.lemmatize("v"))

        for word in lemmatized:
            words.append(word)

    # total_string = ""
    # for item in words:
    #     total_string += (" "+item)
    # print(total_string)
    #
    # blob_reconstructed = TextBlob(total_string)
    # dict = {}
    # for item in blob_reconstructed.words:
    #     dict[item] = blob_reconstructed.word_counts[item]
    # print(dict)
    return words

if __name__ == "__main__":
    tweets = get_replies_to_candidate("EmmanuelMacron")
    get_unique_words(tweets)
