import twitter_connection_setup as connect
import candidate_queries as request
from collect_candidate_actuality_tweets import *

from dataframe import *
import pandas as pd
from matplotlib import pyplot as plt

connexion = connect.twitter_setup()
queries = request.get_candidate_queries(1,"../candidateData")

tweets=get_tweets_from_candidates_search_queries(queries,connexion)
data = convert_2_dataframe(tweets[0])

tfav = pd.Series(data=data['Likes'].values, index=data['date'])
tret = pd.Series(data=data['RTs'].values, index=data['date'])

# Likes vs retweets visualization:
tfav.plot(figsize=(16,4), label="Likes", legend=True)
tret.plot(figsize=(16,4), label="Retweets", legend=True)

plt.show()
