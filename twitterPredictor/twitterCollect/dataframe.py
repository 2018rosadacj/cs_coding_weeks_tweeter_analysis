import pandas as pd
from datetime import datetime
from collect_candidate_tweet_activity import *
import numpy as np
import json

def convert_2_dataframe(data):
    """
    Input: list of tweets
    Output: dataframe
    """
    # with open("EmmanuelMacron.json","r") as file:
    #     data = file.readlines()
    #     for item in data:
    #         item = item[:-1]
    # tweets = []
    # for item in data:
    # #     print(type(item))
    # #     if type(item)=="str":
    #     tweets.append(item._json)
    #     else:
    #         tweets.append(item)
    # print(tweets)
    date = []
    text = []
    hashtags = []
    retweets = []
    likes = []
    number_tweets = 0
    for tweet in data:
        number_tweets +=1
        date.append(tweet.created_at)
        text.append(tweet.text)

        hash_list = []
        for hash in tweet.entities.get("hashtags"):
            hash_list.append("#"+hash.get("text"))

        hashtags.append(hash_list)
        
        retweets.append(tweet.retweet_count)
        likes.append(tweet.favorite_count)


    column = ["date","text","hashtags","RTs","Likes"]
    date_series = pd.Series(date)
    text_series = pd.Series(text)
    hashtags_series = pd.Series(hashtags)
    rt_series = pd.Series(retweets)
    like_series = pd.Series(likes)

    dataframe = pd.DataFrame({column[0] : date_series, column[1] : text_series, column[2] : hashtags_series,column[3] : rt_series,column[4] : like_series})

    # dataframe = pd.DataFrame(text,index = date, columns = column)
    return dataframe

if __name__ == "__main__":
    tweets = get_replies_to_candidate("EmmanuelMacron")
    data = convert_2_dataframe(tweets)
    print(data.head())
