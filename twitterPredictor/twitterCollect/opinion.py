from collect_candidate_tweet_activity import *
from dataframe import *
from textblob import *

def categorize_tweets(data,neutral_line):
    pos_tweets = []
    neu_tweets = []
    neg_tweets = []

    for item in data["text"]:
        try:
            blob = TextBlob(item)
            blob = blob.translate(to='en')
        except:
            blob = TextBlob(item)

        polarity = blob.sentiment.polarity
        print(blob)
        print(polarity)
        if polarity<=neutral_line and polarity >=-neutral_line:
            neu_tweets.append(item)
        elif polarity > neutral_line:
            pos_tweets.append(item)
        else:
            neg_tweets.append(item)

    return pos_tweets,neu_tweets,neg_tweets

tweets = get_replies_to_candidate("EmmanuelMacron")
data = convert_2_dataframe(tweets)

pos_tweets,neu_tweets,neg_tweets = categorize_tweets(data,0.1)

print("Percentage of positive tweets: {}%".format(len(pos_tweets)*100/len(data['text'])))
print("Percentage of neutral tweets: {}%".format(len(neu_tweets)*100/len(data['text'])))
print("Percentage de negative tweets: {}%".format(len(neg_tweets)*100/len(data['text'])))
